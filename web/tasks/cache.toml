[task]
name = "Cache optimization"
template = "S_templates/cache.S"
cache_max_size = 16

description = '''
# Cache optimization.
**Write a program that sorts an array using a sorting algorithm of your choice.**

The size of the array will be located at the address `array_size`, the integer array (32-bit integer words) will start
at the address `array_start`.

The program should sort the array in ascending order.
Your program will be tested with different array sizes and different values in the array.

Set the parameters of the cache by using `#pragma cache:policy,sets,words_in_blocks,ways,write_method` on a separate line in your task submission.
For example: `#pragma cache:lru,1,1,1,wb`

This parameter will be passed to qtrvsim_cli as the `--d-cache` parameter.

- The allowed maximal cache capacity is limited to 16 32-bit words.
- The length of the official evaluation dataset to sort is in the range of 24 to 32 words.
- The initial main memory memory access latency is set to 10 cycles.
- The burst latency 2 is configured for the following consecutive accesses.

The complete task description can also be found [here](https://cw.fel.cvut.cz/wiki/courses/b35apo/en/homeworks/bonus/start), Makefile and template files for your own testing can be found on [GitLab](https://gitlab.fel.cvut.cz/b35apo/stud-support/-/tree/master/seminaries/qtrvsim/apo-sort).
'''

[arguments]
run = "--dump-cycles --cycle-limit 10000 --read-time 10 --write-time 10 --burst-time 2"

[[inputs]]
data_in = "Size of the array located at address array_size, the array start is located at the address array_start."
data_out = "Sorted array of length array_size"
description = "Bubble sort algorithm."

[[testcases]]
name = "5 elements"

[[testcases.starting_mem]]
array_size = [5]
array_start = [1, 3, 4, 5, 2]

[[testcases.reference_mem]]
array_start = [1, 2, 3, 4, 5]

[[testcases]]
name = "10 elements"

[[testcases.starting_mem]]
array_size = [10]
array_start = [1, 3, 4, 5, 2, 7, 9, 8, 6, 10]

[[testcases.reference_mem]]
array_start = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

[[testcases]]
name = "25 elements"

[[testcases.starting_mem]]
array_size = [25]
array_start = [1, 3, 4, 5, 2, 7, 9, 8, 6, 10, 11, 13, 14, 15, 12, 17, 19, 18, 16, 20, 21, 23, 24, 25, 22]

[[testcases.reference_mem]]
array_start = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]

[[testcases]]
name = "private 1"
private = true

[[testcases.starting_mem]]
array_size = [10]
array_start = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]

[[testcases.reference_mem]]
array_start = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

[[testcases]]
name = "private 2"
private = true

[[testcases.starting_mem]]
array_size = [7]
array_start = [47, 89, 48, 12, 45, 79, 34]

[[testcases.reference_mem]]
array_start = [12, 34, 45, 47, 48, 79, 89]

[[testcases]]
name = "scoring testcase"
private = true

[[testcases.starting_mem]]
array_size = [23]
array_start = [49789, 45633, 448, 1, 447, 9986, 1125, 44, 4, 8, 96, 2, 9, 7789, 44444, 55555, 66666, 99999, 77777, 6325, 1258, 12345, 54321]

[[testcases.reference_mem]]
array_start = [1, 2, 4, 8, 9, 44, 96, 447, 448, 1125, 1258, 6325, 7789, 9986, 12345, 44444, 45633, 49789, 54321, 55555, 66666, 77777, 99999]

[score]
description = "Runtime of the program in cycles."
testcase = "scoring testcase"
