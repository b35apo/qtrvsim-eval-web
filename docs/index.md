---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "QtRVSim Web Eval"
  text: "Wiki page"
  image:
      src: /favicon.png
      alt: QtRVSim WebEval logo
  tagline: "Improve your understanding of RISC-V assembly"
  #actions:
    #- theme: brand
  #  - theme: brand
  #    text: Enter
  #    link: /WebEval
  #  - theme: alt
  #    text: User Manual
  #    link: /WebEval/user
  #  - theme: alt
  #    text: Developer Manual
  #    link: /WebEval/dev

features:
  - title: Wiki
    icon: ω
    details: Welcome to the QtRVSim Web Evaluator Wiki page.
    link: /WebEval/

  - title: User Manual
    icon: υ
    details: Got stuck? Check the user manual for help.
    link: /WebEval/user/

  - title: Developer Manual
    icon: δ
    details: Want to contribute or deploy the application? Check the developer manual.
    link: /WebEval/dev/
    
    #- theme: alt
    #  text: API Examples
    #  link: /api-examples

#features:
#  - title: Feature A
#    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#  - title: Feature B
#    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#  - title: Feature C
#    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#---

